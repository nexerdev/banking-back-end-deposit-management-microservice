package by.andersen.abank.depositmicroservice.model.dto.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDepositResponseDto {
    Long id;

    String titleDeposit;

    Double rateDeposit;

    String currencyDeposit;

    BigDecimal amount;

    String startDate;

    String endDate;

    Long accountNumberApiUser;

    Long contractNumber;

    Boolean isEarlyTerminationDeposit;

    Boolean isAutomaticRenewal;

    Boolean isPartialWithdrawalDeposit;

    Boolean isReplenishDeposit;

    String card;
}
