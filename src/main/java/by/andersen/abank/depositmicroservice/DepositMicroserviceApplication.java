package by.andersen.abank.depositmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class DepositMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DepositMicroserviceApplication.class, args);
    }

}
