package by.andersen.abank.depositmicroservice.model.entity.deposit;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter

@Entity
@Table(name = "deposit_products")
public class DepositProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title",nullable = false)
    private String title;

    @Column(name = "rate",nullable = false)
    private Double rate;

    @Column(name = "currency",nullable = false)
    private String currency;

    @Column(name = "min_amount",nullable = false)
    private Integer minAmount;

    @Column(name = "max_amount",nullable = false)
    private Integer maxAmount;

    @Column(name = "months",nullable = false)
    private Integer periodInMonths;

    @Column(name = "payment_day",columnDefinition="integer default 15")
    private Integer paymentDay;

    @Column(name = "is_capitalize")
    private Boolean isCapitalize;

    @Column(name = "is_early_termination",nullable = false)
    private Boolean isEarlyTermination;

    @Column(name = "is_partial_withdrawal",nullable = false)
    private Boolean isPartialWithdrawal;

    @Column(name = "is_replenish",nullable = false)
    private Boolean isReplenish;

}
