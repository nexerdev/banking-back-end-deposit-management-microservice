package by.andersen.abank.depositmicroservice.utils;

import by.andersen.abank.depositmicroservice.exception.constant.ExceptionMessage;
import by.andersen.abank.depositmicroservice.repository.CredentialRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserResolver {

    private final CredentialRepository repository;

    @Transactional
    public Long resolveId() {
        String login = Optional.ofNullable(SecurityContextHolder
                .getContext()
                .getAuthentication())
                .orElseThrow(() -> new UsernameNotFoundException(ExceptionMessage.USER_NOT_FOUND))
                .getName();
        return repository.findUserCredentialByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException(ExceptionMessage.USER_NOT_FOUND))
                .getUser().getId();
    }
}
