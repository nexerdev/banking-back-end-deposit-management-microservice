package by.andersen.abank.depositmicroservice.repository;

import by.andersen.abank.depositmicroservice.model.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}

