package by.andersen.abank.depositmicroservice.config.swagger.annotation;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static by.andersen.abank.depositmicroservice.config.swagger.Constant.ControllerResponseStatus.*;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ApiResponses(value = {
        @ApiResponse(responseCode = "401", description = STATUS_CODE_UNAUTHORIZED, content = @Content),
        @ApiResponse(responseCode = "404", description = STATUS_CODE_NOT_FOUND, content = @Content),
        @ApiResponse(responseCode = "500", description = STATUS_CODE_INTERNAL_SERVER_ERROR, content = @Content)
})
public @interface SwaggerCardApiResponseStatusConfiguration {

}
