package by.andersen.abank.depositmicroservice.repository;

import by.andersen.abank.depositmicroservice.model.entity.deposit.UserDeposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDepositRepository extends JpaRepository<UserDeposit, Long> {
    List<UserDeposit> findUserDepositsByUserId(Long id);

}
