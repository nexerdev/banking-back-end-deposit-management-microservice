package by.andersen.abank.depositmicroservice.config.swagger.Constant;

public class ControllerResponseStatus {

    public static final String STATUS_CODE_UNAUTHORIZED = "Unauthorized";
    public static final String STATUS_CODE_NOT_FOUND = "Not found";
    public static final String STATUS_CODE_INTERNAL_SERVER_ERROR = "Internal server error";
}
