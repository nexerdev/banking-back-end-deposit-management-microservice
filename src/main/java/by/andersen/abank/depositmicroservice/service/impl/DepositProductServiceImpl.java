package by.andersen.abank.depositmicroservice.service.impl;

import by.andersen.abank.depositmicroservice.model.dto.response.DepositProductResponseDto;
import by.andersen.abank.depositmicroservice.model.entity.deposit.DepositProduct;
import by.andersen.abank.depositmicroservice.repository.DepositProductRepository;
import by.andersen.abank.depositmicroservice.service.DepositProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class DepositProductServiceImpl implements DepositProductService {
    @Autowired
    DepositProductRepository depositProductRepository;

    @Override
    public List<DepositProductResponseDto> findAllDeposits() {

        List<DepositProduct> depositProducts = depositProductRepository.findAll();
        return depositProducts.stream()
                .map(deposit -> DepositProductResponseDto.builder()
                        .id(deposit.getId())
                        .currency(deposit.getCurrency())
                        .isEarlyTermination(deposit.getIsEarlyTermination())
                        .isPartialWithdrawal(deposit.getIsPartialWithdrawal())
                        .isReplenish(deposit.getIsReplenish())
                        .maxAmount(deposit.getMaxAmount())
                        .minAmount(deposit.getMinAmount())
                        .periodInMonths(deposit.getPeriodInMonths())
                        .isCapitalize(deposit.getIsCapitalize())
                        .paymentDay(deposit.getPaymentDay())
                        .rate(deposit.getRate())
                        .title(deposit.getTitle())
                        .build()
                ).collect(Collectors.toList());
    }
}
