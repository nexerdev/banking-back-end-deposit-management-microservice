package by.andersen.abank.depositmicroservice.repository;

import by.andersen.abank.depositmicroservice.model.entity.deposit.DepositProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;




@Repository
public interface DepositProductRepository extends JpaRepository<DepositProduct, Long> {
}

