package by.andersen.abank.depositmicroservice.service;

import by.andersen.abank.depositmicroservice.model.dto.response.DepositProductResponseDto;

import java.util.List;

public interface DepositProductService {
    List<DepositProductResponseDto> findAllDeposits();
}
