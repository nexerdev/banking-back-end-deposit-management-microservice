package by.andersen.abank.depositmicroservice.config.security;

import by.andersen.abank.depositmicroservice.exception.constant.ExceptionMessage;
import by.andersen.abank.depositmicroservice.model.entity.user.Credential;
import by.andersen.abank.depositmicroservice.repository.CredentialRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final CredentialRepository credentialRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Credential credential = credentialRepository
                .findUserCredentialByLogin(login)
                .orElseThrow(() -> new UsernameNotFoundException(ExceptionMessage.USER_NOT_FOUND));
        return User
                .withUsername(login)
                .password(credential.getPassword())
                .authorities(credentialRepository.findRoleByUserId(credential.getId()))
                .accountExpired(false)
                .accountLocked(false)
                .disabled(false)
                .credentialsExpired(false)
                .build();
    }
}