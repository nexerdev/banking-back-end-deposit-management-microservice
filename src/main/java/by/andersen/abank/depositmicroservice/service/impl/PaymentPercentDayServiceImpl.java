package by.andersen.abank.depositmicroservice.service.impl;

import by.andersen.abank.depositmicroservice.model.entity.deposit.UserDeposit;
import by.andersen.abank.depositmicroservice.repository.UserDepositRepository;
import by.andersen.abank.depositmicroservice.service.PaymentPercentDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;

import static java.time.DayOfWeek.*;


@Component
@EnableScheduling
public class PaymentPercentDayServiceImpl implements PaymentPercentDayService {
    @Autowired
    UserDepositRepository userDepositRepository;
    private  List<UserDeposit> allUserDeposits;

    @Scheduled(cron = "0 0 9 * * ?")
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    @Override
    public void paymentPercent() {
        allUserDeposits = userDepositRepository.findAll();

       for (int i = 0; i < allUserDeposits.size(); i++) {
           int paymentDay = allUserDeposits.get(i).getDepositProduct().getPaymentDay();

           if(isPaymentDay(paymentDay)){
               if(allUserDeposits.get(i).getDepositProduct().getIsCapitalize()){
                   calcPercentAmount(allUserDeposits.get(i));
                   userDepositRepository.save(allUserDeposits.get(i));
               }else{
                   // send money on card client isCapitalize
               }
           }
       }
    }

    public boolean isPaymentDay(Integer paymentDay){
        LocalDate date = LocalDate.now();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        int dayOfMonth = date.getDayOfMonth();

        if(dayOfWeek != SATURDAY && dayOfWeek != SUNDAY && dayOfMonth == paymentDay
                || dayOfWeek == MONDAY && (date.minusDays(1).getDayOfMonth() == paymentDay || date.minusDays(2).getDayOfMonth() == paymentDay)){
            return true;
        }else{
            return false;
        }
    }

    public  void calcPercentAmount(UserDeposit userDeposit){
        BigDecimal amount = userDeposit.getAmount();
        Double rate = userDeposit.getDepositProduct().getRate();
        BigDecimal amountWithPercent = amount.multiply(BigDecimal.valueOf(rate/100 + 1));
        userDeposit.setAmount(amountWithPercent);
    }

}
