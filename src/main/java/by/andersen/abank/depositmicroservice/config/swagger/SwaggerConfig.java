package by.andersen.abank.depositmicroservice.config.swagger;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.security.SecuritySchemes;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@SecuritySchemes(value = @SecurityScheme(name = "TOKEN", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT"))
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("a-bank-public")
                .packagesToScan("by.andersen.abank")
                .build();
    }

    @Bean
    public OpenAPI affinityBankOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("Affinity Bank deposit API")
                        .description("Affinity Bank application by andersenlab.com")
                        .version("v0.0.3")).externalDocs(new ExternalDocumentation()
                        .description("Affinity Bank Wiki Documentation")
                        .url("https://wiki.andersenlab.com/display/VMN/Onboarding"));
    }
}
