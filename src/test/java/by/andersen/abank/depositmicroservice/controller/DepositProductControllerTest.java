package by.andersen.abank.depositmicroservice.controller;

import by.andersen.abank.depositmicroservice.model.dto.request.AutomaticRenewalRequestDto;
import by.andersen.abank.depositmicroservice.model.dto.response.DepositProductResponseDto;
import by.andersen.abank.depositmicroservice.model.dto.response.UserDepositResponseDto;
import by.andersen.abank.depositmicroservice.service.DepositProductService;
import by.andersen.abank.depositmicroservice.service.UserDepositService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DepositProductControllerTest {
    @Mock
    private DepositProductService depositProductService;

    @Mock
    private UserDepositService userDepositService;

    private AutomaticRenewalRequestDto automaticRenewalRequestDto;
    private List<DepositProductResponseDto> listDepositProductsResponseDto;
    private List<UserDepositResponseDto> listUserDepositResponseDto;

    @BeforeEach
    void beforeTests() {
        DepositProductResponseDto depositProductResponseDto = DepositProductResponseDto.builder()
                .build();
        listDepositProductsResponseDto = new ArrayList<>();
        listDepositProductsResponseDto.add(depositProductResponseDto);

        UserDepositResponseDto userDepositResponseDto = UserDepositResponseDto.builder().build();
        listUserDepositResponseDto = new ArrayList<>();
        listUserDepositResponseDto.add(userDepositResponseDto);

        automaticRenewalRequestDto = AutomaticRenewalRequestDto.builder()
                .isAtomaticRenewal(true)
                .idUserDeposit(1l)
                .build();
    }

    @Test
    void getDepositProductsPositiveCase(){
        when(depositProductService.findAllDeposits())
                .thenReturn(listDepositProductsResponseDto);
        assertThat(depositProductService.findAllDeposits())
                .isEqualTo(listDepositProductsResponseDto);
    }

    @Test
    void getUserDepositsPositiveCase(){
        when(userDepositService.getClientDeposits(1L))
                .thenReturn(listUserDepositResponseDto);
        assertThat(userDepositService.getClientDeposits(1L))
                .isEqualTo(listUserDepositResponseDto);
    }

}
