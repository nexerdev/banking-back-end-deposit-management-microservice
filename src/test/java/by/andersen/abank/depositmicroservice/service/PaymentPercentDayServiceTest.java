package by.andersen.abank.depositmicroservice.service;

import by.andersen.abank.depositmicroservice.model.entity.deposit.DepositProduct;
import by.andersen.abank.depositmicroservice.model.entity.deposit.UserDeposit;
import by.andersen.abank.depositmicroservice.model.entity.user.User;
import by.andersen.abank.depositmicroservice.service.impl.PaymentPercentDayServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;

import static java.time.DayOfWeek.*;
import static org.assertj.core.api.Assertions.assertThat;

public class PaymentPercentDayServiceTest {
    private PaymentPercentDayServiceImpl paymentPercentDayService;
    private User user ;
    private DepositProduct depositProduct;

    @Mock
    UserDeposit userDeposit;

    @BeforeEach
    void beforeEachTest(){
        paymentPercentDayService = new PaymentPercentDayServiceImpl();
        user = User.builder().build();
        depositProduct = DepositProduct.builder()
                .title("123")
                .currency("EUR")
                .isEarlyTermination(true)
                .isPartialWithdrawal(false)
                .isReplenish(false)
                .minAmount(100)
                .maxAmount(122)
                .rate(10.0)
                .periodInMonths(2)
                .build();
        userDeposit = UserDeposit.builder()
                .amount(BigDecimal.valueOf(100))
                .cardNumber("3344")
                .contractNumber(123123L)
                .isAutomaticRenewal(true)
                .startDate("2011-11-01")
                .endDate("2021-11-11")
                .depositProduct(depositProduct)
                .user(user)
                .build();
    }



    @Test
    void isPaymentDayTestPositive (){
        LocalDate date = LocalDate.now();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        int dayOfMonth = date.getDayOfMonth();

        if(dayOfWeek != SATURDAY && dayOfWeek != SUNDAY && dayOfMonth == 5
                || dayOfWeek == MONDAY && (date.minusDays(1).getDayOfMonth() == 5 || date.minusDays(2).getDayOfMonth() == 5)){

            assertThat( paymentPercentDayService.isPaymentDay(5)).isEqualTo(true);
        }
    }
    @Test
    void isPaymentDayTestNegative (){
        LocalDate date = LocalDate.now();
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        int dayOfMonth = date.getDayOfMonth();

        if(dayOfWeek == SATURDAY && dayOfWeek == SUNDAY && dayOfMonth != 5){
            assertThat( paymentPercentDayService.isPaymentDay(6)).isEqualTo(false);
        }
    }

    @Test
    void calcPercentAmountTest(){
        paymentPercentDayService.calcPercentAmount(userDeposit);
        assertThat(userDeposit.getAmount()).isEqualTo(BigDecimal.valueOf(110.0));
    }
}
