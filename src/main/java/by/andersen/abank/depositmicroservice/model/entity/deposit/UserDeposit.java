package by.andersen.abank.depositmicroservice.model.entity.deposit;

import by.andersen.abank.depositmicroservice.model.entity.user.User;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "user_deposit")
public class UserDeposit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;


    @Column(name = "amount", precision = 20, scale = 2)
    private BigDecimal amount = BigDecimal.ZERO;

    @Column(name = "start_date",nullable = false)
    private String startDate;

    @Column(name = "end_date",nullable = false)
    private String endDate;

    @Column(name = "card_number",nullable = false)
    private String cardNumber;

    @Column(name = "contract_number",nullable = false)
    private Long contractNumber;

    @Column(name = "is_automatic_renewal",nullable = false)
    private Boolean isAutomaticRenewal;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "deposit_product_id",nullable = false)
    private DepositProduct depositProduct;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id",nullable = false)
    private User user;
}
