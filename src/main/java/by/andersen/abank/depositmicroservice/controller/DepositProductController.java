package by.andersen.abank.depositmicroservice.controller;


import by.andersen.abank.depositmicroservice.config.swagger.annotation.SwaggerCardApiResponseStatusConfiguration;
import by.andersen.abank.depositmicroservice.model.dto.request.AddUserDepositRequestDto;
import by.andersen.abank.depositmicroservice.model.dto.request.AutomaticRenewalRequestDto;
import by.andersen.abank.depositmicroservice.model.dto.response.DepositProductResponseDto;
import by.andersen.abank.depositmicroservice.model.dto.response.UserDepositResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Tag(name = "Deposit products controller")
@RequestMapping(value = "/a-banking/deposits")
@SwaggerCardApiResponseStatusConfiguration
public interface DepositProductController {

    @Operation(summary = "Get all deposit products")
    @RequestMapping(value = "/deposit-products",method = RequestMethod.GET)
    ResponseEntity<List<DepositProductResponseDto>> getDepositProducts();

    @Operation(summary = "Get all user deposits")
    @RequestMapping(value = "/user-deposits",method = RequestMethod.GET)
    ResponseEntity<List<UserDepositResponseDto>> getUserDeposits();

    @Operation(summary = "Change boolen field IsAutomaticRenewal")
    @RequestMapping(value = "/user-deposits/automatic-renewal", method = RequestMethod.PATCH)
    ResponseEntity<Void> chageIsAutomaticRenewal(@RequestBody AutomaticRenewalRequestDto automaticRenewalRequestDto);

    @Operation(summary = "Add a new user deposit")
    @RequestMapping(value = "/create-deposit/", method = RequestMethod.POST)
    ResponseEntity<Void> addUserDeposit(@RequestBody AddUserDepositRequestDto addUserDepositRequestDto);
}
