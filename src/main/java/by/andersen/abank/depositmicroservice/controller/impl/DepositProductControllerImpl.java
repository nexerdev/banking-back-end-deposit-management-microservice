package by.andersen.abank.depositmicroservice.controller.impl;

import by.andersen.abank.depositmicroservice.controller.DepositProductController;
import by.andersen.abank.depositmicroservice.model.dto.request.AddUserDepositRequestDto;
import by.andersen.abank.depositmicroservice.model.dto.request.AutomaticRenewalRequestDto;
import by.andersen.abank.depositmicroservice.model.dto.response.DepositProductResponseDto;
import by.andersen.abank.depositmicroservice.model.dto.response.UserDepositResponseDto;
import by.andersen.abank.depositmicroservice.service.DepositProductService;
import by.andersen.abank.depositmicroservice.service.UserDepositService;
import by.andersen.abank.depositmicroservice.utils.UserResolver;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SecurityRequirement(name = "TOKEN")
@PreAuthorize("isAuthenticated()")
@RestController
@RequiredArgsConstructor
public class DepositProductControllerImpl implements DepositProductController {
    @Autowired
    private DepositProductService depositProductService;
    @Autowired
    private UserDepositService userDepositService;
    @Autowired
    private UserResolver userResolver;

    @Override
    public ResponseEntity<List<DepositProductResponseDto>> getDepositProducts() {
        return new ResponseEntity<>(depositProductService.findAllDeposits(), HttpStatus.OK);

    }

    @Override
    public ResponseEntity<List<UserDepositResponseDto>> getUserDeposits() {
        return new ResponseEntity<>(userDepositService.getClientDeposits(userResolver.resolveId()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> chageIsAutomaticRenewal(AutomaticRenewalRequestDto automaticRenewalRequestDto) {
        userDepositService.updateIsAutomaticRenewal(automaticRenewalRequestDto);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> addUserDeposit(AddUserDepositRequestDto addUserDepositRequestDto) {
        userDepositService.addUserDeposit(addUserDepositRequestDto);
        return ResponseEntity.ok().build();
    }


}
