package by.andersen.abank.depositmicroservice.model.entity.user;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"id"})
@EqualsAndHashCode(exclude = {"id"})
@Builder
@Getter
@Setter

@Entity
@Table(name = "credential")
public class Credential{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "phone_login",nullable = false)
    private String phoneLogin;

    @Column(name = "passport_login",nullable = false)
    private String passportLogin;

    @Column(name = "password",nullable = false)
    private String password;

    @Column(name = "registration_date",nullable = false)
    private Date registrationDate;

    @Column(name = "last_login_date",nullable = false)
    private Date lastLoginDate;

    @Column(name = "secret_question",nullable = false)
    private String secretQuestion;

    @Column(name = "secret_question_answer",nullable = false)
    private String secretQuestionAnswer;

    @Column(name = "is_active",nullable = false)
    private Boolean isActive;

    @Column(name = "is_non_locked",nullable = false)
    private Boolean isNonLocked;

    @Column(name = "is_credential_non_expired",nullable = false)
    private Boolean isCredentialNonExpired;

    @OneToOne
    @JoinColumn(name = "user_id",nullable = false)
    private User user;
}
