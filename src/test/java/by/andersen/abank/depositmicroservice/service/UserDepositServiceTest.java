package by.andersen.abank.depositmicroservice.service;


import by.andersen.abank.depositmicroservice.exception.DepositProductNotFound;
import by.andersen.abank.depositmicroservice.exception.constant.ExceptionMessage;
import by.andersen.abank.depositmicroservice.model.entity.deposit.DepositProduct;
import by.andersen.abank.depositmicroservice.model.entity.deposit.UserDeposit;
import by.andersen.abank.depositmicroservice.model.entity.user.User;
import by.andersen.abank.depositmicroservice.repository.DepositProductRepository;
import by.andersen.abank.depositmicroservice.repository.UserDepositRepository;
import by.andersen.abank.depositmicroservice.repository.UserRepository;
import by.andersen.abank.depositmicroservice.service.impl.UserDepositServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserDepositServiceTest {
    @Mock
    private UserDepositRepository userDepositRepository;

    private  List<UserDeposit> listUserDeposits;
    private User user ;
    private DepositProduct depositProduct;
    private UserDeposit userDeposit;
    private  UserDepositServiceImpl userDepositServiceImpl;
    @BeforeEach
    void beforeEachTest() {
        user = User.builder().build();
        depositProduct = DepositProduct.builder()
                .title("123")
                .currency("EUR")
                .isEarlyTermination(true)
                .isPartialWithdrawal(false)
                .isReplenish(false)
                .minAmount(100)
                .maxAmount(122)
                .rate(20.3)
                .periodInMonths(2)
                .build();
        userDeposit = UserDeposit.builder()
                .amount(BigDecimal.valueOf(20.4))
                .cardNumber("123123")
                .contractNumber(123123L)
                .isAutomaticRenewal(true)
                .startDate("2011-11-01")
                .endDate("2021-11-11")
                .depositProduct(depositProduct)
                .user(user)
                .build();

        listUserDeposits = new ArrayList<>();
        listUserDeposits.add(userDeposit);

        userDepositServiceImpl = new UserDepositServiceImpl();
    }

    @Test
    void getClientDepositsTest(){
        when(userDepositRepository.findUserDepositsByUserId(1l))
                .thenReturn(listUserDeposits);

        assertThat(userDepositRepository.findUserDepositsByUserId(1l))
                .isEqualTo(listUserDeposits);
    }

    @Test
    void addUserDepositTest(){
        when(userDepositRepository.save(userDeposit))
                .thenReturn(userDeposit);

        assertThat(userDepositRepository.save(userDeposit))
                .isEqualTo(userDeposit);

    }
    @Test
    void generateContractNumberForUserDepositTest(){
        Long res = userDepositServiceImpl.generateContractNumberForUserDeposit(1l);
        assertThat(res).isNotNegative();
    }

    @Test
    void getCalculateEndDateTest(){
        assertThat("2022-12-11").isEqualTo(userDepositServiceImpl.getCalculateEndDate("2022-11-11",1));
    }

}
