package by.andersen.abank.depositmicroservice.service;


import by.andersen.abank.depositmicroservice.model.entity.deposit.DepositProduct;
import by.andersen.abank.depositmicroservice.repository.DepositProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DepositProductServiceTest {
    @Mock
    private DepositProductRepository depositProductRepository;

    private List<DepositProduct> depositProducts;

    @BeforeEach
    void beforeEachTest() {
        DepositProduct  depositProduct = DepositProduct.builder()
                .title("123")
                .currency("EUR")
                .isEarlyTermination(true)
                .isPartialWithdrawal(false)
                .isReplenish(false)
                .minAmount(100)
                .maxAmount(122)
                .isCapitalize(true)
                .paymentDay(15)
                .rate(20.3)
                .periodInMonths(2)
                .build();
        depositProducts = new ArrayList<>();
        depositProducts.add(depositProduct);
    }

    @Test
    public void findAllDepositsPositiveCase() {
        when(depositProductRepository.findAll())
                .thenReturn(depositProducts);

        assertThat(depositProductRepository.findAll())
                .isEqualTo(depositProducts);
    }


}
