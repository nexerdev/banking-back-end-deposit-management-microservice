package by.andersen.abank.depositmicroservice.model.dto.request;

import by.andersen.abank.depositmicroservice.model.entity.deposit.DepositProduct;
import by.andersen.abank.depositmicroservice.model.entity.user.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddUserDepositRequestDto {
    @JsonProperty("amount")
    private BigDecimal amount;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("card_number")
    private String cardNumber;

    @JsonProperty("is_automatic_renewal")
    private Boolean isAutomaticRenewal;

    @JsonProperty("deposit_product_id")
    private Long depositProductId;

    @JsonProperty("user_id")
    private Long userId;
}
