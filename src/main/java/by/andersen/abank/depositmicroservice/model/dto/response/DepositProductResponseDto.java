package by.andersen.abank.depositmicroservice.model.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DepositProductResponseDto {
    Long id;

    String title;

    Double rate;

    String currency;

    Integer minAmount;

    Integer maxAmount;

    Integer periodInMonths;

    Integer paymentDay;

    Boolean isCapitalize;

    Boolean isEarlyTermination;

    Boolean isPartialWithdrawal;

    Boolean isReplenish;
}
