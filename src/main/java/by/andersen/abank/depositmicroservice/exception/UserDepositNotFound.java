package by.andersen.abank.depositmicroservice.exception;


public class UserDepositNotFound extends RuntimeException {

    public UserDepositNotFound(String message) {
        super(message);
    }

    public UserDepositNotFound(Throwable cause) {
        super(cause);
    }
}
