package by.andersen.abank.depositmicroservice.service.impl;

import by.andersen.abank.depositmicroservice.exception.DepositProductNotFound;
import by.andersen.abank.depositmicroservice.exception.UserDepositNotFound;
import by.andersen.abank.depositmicroservice.exception.UserNotFound;
import by.andersen.abank.depositmicroservice.exception.constant.ExceptionMessage;
import by.andersen.abank.depositmicroservice.model.dto.request.AddUserDepositRequestDto;
import by.andersen.abank.depositmicroservice.model.dto.request.AutomaticRenewalRequestDto;
import by.andersen.abank.depositmicroservice.model.dto.response.UserDepositResponseDto;
import by.andersen.abank.depositmicroservice.model.entity.deposit.DepositProduct;
import by.andersen.abank.depositmicroservice.model.entity.deposit.UserDeposit;
import by.andersen.abank.depositmicroservice.model.entity.user.User;
import by.andersen.abank.depositmicroservice.repository.DepositProductRepository;
import by.andersen.abank.depositmicroservice.repository.UserDepositRepository;
import by.andersen.abank.depositmicroservice.repository.UserRepository;
import by.andersen.abank.depositmicroservice.service.UserDepositService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UserDepositServiceImpl implements UserDepositService {
    @Autowired
    UserDepositRepository userDepositRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    DepositProductRepository depositProductRepository;

    @Override
    public List<UserDepositResponseDto> getClientDeposits(Long id) {
        List<UserDeposit> depositProducts = userDepositRepository.findUserDepositsByUserId(id);

        return depositProducts.stream()
                .map(userDeposit -> UserDepositResponseDto.builder()
                        .id(userDeposit.getId())
                        .titleDeposit(userDeposit.getDepositProduct().getTitle())
                        .rateDeposit(userDeposit.getDepositProduct().getRate())
                        .currencyDeposit(userDeposit.getDepositProduct().getCurrency())
                        .amount(userDeposit.getAmount())
                        .startDate(userDeposit.getStartDate())
                        .endDate(userDeposit.getEndDate())
                        .accountNumberApiUser(userDeposit.getUser().getUid())
                        .contractNumber(userDeposit.getContractNumber())
                        .isEarlyTerminationDeposit(userDeposit.getDepositProduct().getIsEarlyTermination())
                        .isAutomaticRenewal(userDeposit.getIsAutomaticRenewal())
                        .isPartialWithdrawalDeposit(userDeposit.getDepositProduct().getIsPartialWithdrawal())
                        .isReplenishDeposit(userDeposit.getDepositProduct().getIsReplenish())
                        .card(userDeposit.getCardNumber())
                        .build()
                ).collect(Collectors.toList());
    }

    @Override
    public void updateIsAutomaticRenewal(AutomaticRenewalRequestDto requestDto) {
        UserDeposit userDeposit = userDepositRepository
                .findById(requestDto.getIdUserDeposit())
                .orElseThrow(() -> new UserDepositNotFound(ExceptionMessage.DEPOSIT_NOT_FOUND));

        userDeposit.setIsAutomaticRenewal(requestDto.getIsAtomaticRenewal());
        userDepositRepository.save(userDeposit);
    }

    @Override
    public void addUserDeposit(AddUserDepositRequestDto addUserDepositRequestDto) {
        User user = userRepository.findById(addUserDepositRequestDto.getUserId())
                .orElseThrow(() -> new UserNotFound(ExceptionMessage.USER_NOT_FOUND));

        DepositProduct depositProduct = depositProductRepository.findById(addUserDepositRequestDto.getDepositProductId())
                .orElseThrow(() -> new DepositProductNotFound(ExceptionMessage.DEPOSIT_NOT_FOUND));


        UserDeposit userDeposit = UserDeposit.builder()
                .amount(addUserDepositRequestDto.getAmount())
                .cardNumber(addUserDepositRequestDto.getCardNumber())
                .contractNumber(generateContractNumberForUserDeposit(user.getId()))
                .isAutomaticRenewal(addUserDepositRequestDto.getIsAutomaticRenewal())
                .startDate(addUserDepositRequestDto.getStartDate())
                .endDate(getCalculateEndDate(addUserDepositRequestDto.getStartDate(),depositProduct.getPeriodInMonths()))
                .depositProduct(depositProduct)
                .user(user)
                .build();

                userDepositRepository.save(userDeposit);
    }

    public Long generateContractNumberForUserDeposit(Long userId){
        Random rnd = new Random();
        int number = rnd.nextInt(999999999);
        String res = String.valueOf(number) + String.valueOf(userId);
        return Long.valueOf(res);
    }

    public String getCalculateEndDate(String startDate,Integer month){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate  localDate = LocalDate.parse(startDate, formatter);
        String endDate = String.valueOf(localDate.plusMonths(month));
        return endDate;
    }

}
