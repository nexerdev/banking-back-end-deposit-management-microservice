package by.andersen.abank.depositmicroservice.model.dto.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AutomaticRenewalRequestDto {
    @JsonProperty("id")
    Long idUserDeposit;

    @JsonProperty("is_automatic_renewal")
    Boolean isAtomaticRenewal;
}
