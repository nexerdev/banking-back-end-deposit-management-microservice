package by.andersen.abank.depositmicroservice.model.entity.user;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
@Builder
@Getter
@Setter

@Entity
@Table(name = "passport")
public class Passport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "lastname", length = 64,nullable = false)
    private String lastname;

    @Column(name = "firstname", length = 64,nullable = false)
    private String firstname;

    @Column(name = "surname", length = 64,nullable = false)
    private String surname;
}
