package by.andersen.abank.depositmicroservice.exception;


public class UserNotFound extends RuntimeException {
    public UserNotFound(String message) {
        super(message);
    }

    public UserNotFound(Throwable cause) {
        super(cause);
    }
}
