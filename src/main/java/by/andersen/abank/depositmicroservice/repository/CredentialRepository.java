package by.andersen.abank.depositmicroservice.repository;

import by.andersen.abank.depositmicroservice.model.entity.user.Credential;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CredentialRepository extends JpaRepository<Credential, Long> {

    @Query("select c from Credential c where c.phoneLogin = ?1 or c.passportLogin = ?1")
    Optional<Credential> findUserCredentialByLogin(String login);

    @Query(value = "select user_role_name " +
            "from Credential c " +
            "inner join api_user au on au.id = c.user_id " +
            "inner join user_role ur on ur.id = au.role_id " +
            "where c.id = ?1 ", nativeQuery = true)
    String findRoleByUserId(Long id);
}
