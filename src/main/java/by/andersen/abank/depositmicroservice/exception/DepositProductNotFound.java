package by.andersen.abank.depositmicroservice.exception;

public class DepositProductNotFound extends RuntimeException {

    public DepositProductNotFound(String message) {
        super(message);
    }

    public DepositProductNotFound(Throwable cause) {
        super(cause);
    }
}
