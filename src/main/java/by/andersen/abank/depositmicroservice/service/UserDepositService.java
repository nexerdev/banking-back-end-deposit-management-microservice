package by.andersen.abank.depositmicroservice.service;

import by.andersen.abank.depositmicroservice.model.dto.request.AddUserDepositRequestDto;
import by.andersen.abank.depositmicroservice.model.dto.request.AutomaticRenewalRequestDto;
import by.andersen.abank.depositmicroservice.model.dto.response.UserDepositResponseDto;

import java.util.List;

public interface UserDepositService {
    List<UserDepositResponseDto> getClientDeposits(Long id);

    void updateIsAutomaticRenewal(AutomaticRenewalRequestDto requestDto);

    void addUserDeposit(AddUserDepositRequestDto addUserDepositRequestDto);
}
