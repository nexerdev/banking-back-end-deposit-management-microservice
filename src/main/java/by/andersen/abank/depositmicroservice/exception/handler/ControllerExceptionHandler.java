package by.andersen.abank.depositmicroservice.exception.handler;

import by.andersen.abank.depositmicroservice.exception.DepositProductNotFound;
import by.andersen.abank.depositmicroservice.exception.UserDepositNotFound;
import by.andersen.abank.depositmicroservice.exception.UserNotFound;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return new ResponseEntity<>(errors, BAD_REQUEST);
    }

    @ExceptionHandler(value = AuthenticationException.class)
    ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        log.info("Called method - [handleAuthenticationException]");
        return new ResponseEntity<>(e.getMessage(), UNAUTHORIZED);
    }

    @ExceptionHandler(value = UsernameNotFoundException.class)
    ResponseEntity<String> handleUsernameNotFoundException(UsernameNotFoundException e) {
        log.info("CALLED METHOD - [handleUsernameNotFoundException]");
        return new ResponseEntity<>(e.getMessage(), UNAUTHORIZED);
    }

    @ExceptionHandler(value = DataIntegrityViolationException.class)
    ResponseEntity<String> handleDataIntegrityViolationException(DataIntegrityViolationException e) {
        log.info("CALLED METHOD - [handleDataIntegrityViolationException]");
        return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
    }

    @ExceptionHandler(value = UnsupportedOperationException.class)
    ResponseEntity<String> handleUnsupportedOperationException(UnsupportedOperationException e) {
        log.info("CALLED METHOD - [handleUnsupportedOperationException]");
        return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
    }
    @ExceptionHandler(value = UserDepositNotFound.class)
    ResponseEntity<String> handleCardNotFoundException(UserDepositNotFound e) {
        log.info("CALLED METHOD - [handleCardNotFoundException]");
        return new ResponseEntity<>(e.getMessage(), NOT_FOUND);
    }

    @ExceptionHandler(value = UserNotFound.class)
    ResponseEntity<String> handleUserNotFoundException(UserNotFound e) {
        log.info("CALLED METHOD - [handleUserNotFoundException]");
        return new ResponseEntity<>(e.getMessage(), NOT_FOUND);
    }

    @ExceptionHandler(value = DepositProductNotFound.class)
    ResponseEntity<String> handleDepositProductNotFoundException(DepositProductNotFound e) {
        log.info("CALLED METHOD - [handleDepositProductNotFoundException]");
        return new ResponseEntity<>(e.getMessage(), NOT_FOUND);
    }

    @ExceptionHandler(value = DateTimeParseException.class)
    ResponseEntity<String> handleDateTimeParseException(DateTimeParseException e) {
        log.info("CALLED METHOD - [handleDateTimeParseException]");
        return new ResponseEntity<>(e.getMessage(), CONFLICT);
    }


}
