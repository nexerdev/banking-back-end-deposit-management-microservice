package by.andersen.abank.depositmicroservice.exception.constant;

public class ExceptionMessage {
    public static final String USER_NOT_FOUND = "User not found";
    public static final String DEPOSIT_NOT_FOUND = "Deposit not found";
}
